package eu.chorevolution.vsb.playgrounds.mqtt;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.Queue;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;


public class MqttCallBack implements MqttCallback {
	
	private Queue<byte[]> imagesQueue;
	private Object mutex;
	
	public MqttCallBack(Queue imagesQueue, Object mutex){
		
		this.imagesQueue = imagesQueue;
		this.mutex = mutex;
		
	}
	  public void connectionLost(Throwable throwable) {
		    
		  	System.out.println("Connection to MQTT broker lost!");
		  }

		  public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
			  
			  byte[] message = mqttMessage.getPayload();
			  print(message);
			  if(message.length != 0){
					
					synchronized(mutex){
						
						imagesQueue.add(message);
						print(message);
						mutex.notify();
					}
				
			  }
		  }

		  public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
		    // not used in this example
		  }
		  
		  
		  public void print(final byte[] tabBytes){
				
				//
				System.out.println("Starting printByteTab");
				StringBuffer sb = new StringBuffer();
				sb.append("START \n");

				for (int i = 0; i < 100; i++) {
					sb.append(tabBytes[i] + " | ");
				}

				System.out.println(sb.toString());
			}
		  
		  

}
