package eu.chorevolution.vsb.playgrounds.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;


public class SubscriberMqtt {
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MqttClient client;
		try {
			
			client = new MqttClient("tcp://128.93.64.1:1883", MqttClient.generateClientId());
			client.setCallback( new MyMqttCallBack() );
			client.connect();
			client.subscribe("MIMOVE/image");
			if(client.isConnected()){
				
				System.out.println(client.getClientId());
			}else{
				
				System.out.println("Not connected");
			}
			
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
