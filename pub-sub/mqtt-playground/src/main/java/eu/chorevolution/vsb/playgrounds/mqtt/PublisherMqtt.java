package eu.chorevolution.vsb.playgrounds.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.simple.JSONObject;

import eu.chorevolution.vsb.tools.monitor.agent.MeasureAgent;
import eu.chorevolution.vsb.tools.monitor.util.MonitorConstant;


public class PublisherMqtt {
	
	private String url = null;
	MeasureAgent agent = null;
	public PublisherMqtt(String host, String port){
		
		this.url = "tcp://"+host+":"+port;
		agent = new MeasureAgent("timestamp_5", MonitorConstant.MDefault,
				MonitorConstant.timestamp_5_port_listener);
	}
	
	public void publish(String messageContent, String scope){
		MqttClient client = null;
		    
		try{
			
			client = new MqttClient(url, MqttClient.generateClientId());
			client.connect();
			MqttMessage message = new MqttMessage();
			message.setQos(0);
			message.setPayload(messageContent.getBytes());
			client.publish(scope, message);
			String message_id = agent.getMessageID(messageContent);
			agent.fire("" + System.currentTimeMillis() + "-" + message_id);
			client.disconnect();
			
		}catch (MqttException e){
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
