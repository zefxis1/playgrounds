package eu.chorevolution.vsb.playgrounds.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.simple.JSONObject;

public class Publisher {
	
	public static void main(String[] args){
		MqttClient client = null;
		
		JSONObject obj = new JSONObject();
		obj.put("lat", "12584589.78541238");
		obj.put("lon", "84589.78541238");
		obj.put("temp", "84589.78541238");
		obj.put("op_name", "bridgeNextClosure");
        		
        String content = obj.toJSONString();
	        
		try{
			
			client = new MqttClient("tcp://128.93.64.1:1883", MqttClient.generateClientId());
			System.out.println("Before connecting ");
			client.connect();
			System.out.println("After connecting ");
			MqttMessage message = new MqttMessage();
			message.setQos(0);
			message.setPayload(content.getBytes());
			System.out.println("Publish : "+content);
			client.publish("MIMOVE/image", message);
			client.disconnect();
			
		}catch (MqttException e){
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
