package eu.chorevolution.vsb.playgrounds.mqtt;

import java.util.LinkedList;
import java.util.Queue;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

public class Subscriber {
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Queue<byte[]> imagesQueue = new LinkedList<byte[]>();
		Object mutex = new Object();
		MqttClient client;
		try {
			
			client = new MqttClient("tcp://128.93.64.1:1883", MqttClient.generateClientId());
			client.setCallback( new MqttCallBack(imagesQueue, mutex));
			client.connect();
			client.subscribe("MIMOVE/image");
			JFrameDisplayer jFrameDisplayer = new JFrameDisplayer(imagesQueue,mutex);
			jFrameDisplayer.start();
			
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
