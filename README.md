**Playground : Communication style of distributed systems**


**1. Client Server (CS) style**

The Client-Server communication style, is commonly used for Web Services. 
A client communicates directly with a server either by direct messaging (push notifications) or by a remote procedure call (RPC) through an operation. 
In the first case, a single item (which encloses data) is sent from the sending entity (server) to the receiving entity (client), while, in the second case, an exchange takes place between the two entities with a request message followed by a response.

In the actual playground we showcase middleware protocols such as CoAP, SOAP, DPWS, HTTP(REST Architecture)  and ZeroMQ follow the CS style.


**2. Publish Subscribe (Pub-Sub) style**

The Publish-Subscribe communication style, is commonly used for content broadcasting/feeds. In Pub-Sub, multiple peers interact via an intermediate broker entity. 
Publishers produce events characterized by a specific filter to the broker. Subscribers subscribe their interest for specific filters to the broker,
who maintains an up-to-date list of subscriptions. The broker matches received events with subscriptions and delivers a copy of each event to each interested subscriber. 
There are different types of subscription schemes, such as topic-based, content-based and type-based. 

In topic-based Pub-Sub, events are characterized with a topic, and subscribers subscribe to specific topics. 
In content-based Pub-Sub, subscribers provide content filters (conditions on specific attributes of events), and receive only the events that satisfy these conditions. 
Finally, in type-based Pub-Sub, the event structure is abstracted based on specific types and subscribers receive them based on their type.
Regardless of the subscription scheme, we use the generic term filter, which represents the subset of events that each peer is interested to publish/receive.

We implement IoT middleware protocols such as MQTT, JMS and  ZeroMQ that follow the Pub-Sub style. There are middlewre protocols such as AMQP, as well as tools and technologies
such as RabbitMQ, Kafka and  follow the Pub-Sub style.

**3. Data Streaming (DS) style**


The Data Streaming communication style, is commonly used for continuous interactions. Middleware protocols such as Websockets and Dioptase, are based on the DS style. 
IoT applications (e.g., traffic management, warehouse logistic, etc) produce data coming from the physical world. Such information is produced as a flow of structured data (stream) and thus require continuous handling.

In DS, a consumer (typically) establishes a dedicated session using an open stream request, which is sent to the producer. Upon the session’s establishment, a continuous flow of data is pushed from the producer to the consumer.

The Websockets is the middleware protocol implemented so far. 

**4. Tuple Space (TS) style**

The Tuple Space communication style, is commonly used for shared data with multiple read/write peers. In TS, multiple peers interact via an intermediate node with a tuple space (tspace). 
Peers can write (out) data into the tspace and can also synchronously retrieve data from it, either by reading (read) a copy or removing (take) the data. 
Data take the form of tuples; a tuple is an ordered list of typed elements. Data are retrieved by matching based on a tuple template, which may define values or expressions for some of the elements.

Tuple space middleware protocols such as SemiSpace , GigaSpaces, JavaSpaces, are based on the TS style. We implement SemiSpace middleware protocol.
