package eu.chorevolution.vsb.playgrounds.str.websockets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import eu.chorevolution.vsb.playgrounds.str.websocketsImp.WsServer;

import java.sql.Timestamp;

public class StartServerWebSocket {

	public WsServer server = null;
	// private MeasureAgent agent = null;

	public StartServerWebSocket() {

		// create a server listening on port 8090
		server = new WsServer(new InetSocketAddress(8891));
		// agent = new MeasureAgent("timestamp_1", System.currentTimeMillis(),
		// MonitorConstant.M3,
		// MonitorConstant.timestamp_1_port_listener);

	}

	public void start() {

		server.start();

	}

	public void send(String message) {

		try {
			
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject) parser.parse(message.trim());
			long departure_time = System.nanoTime();
			jsonObject.put("time", ""+departure_time+"");
			server.send(jsonObject.toJSONString());

		} catch (ParseException e){e.printStackTrace();}

	}

}
