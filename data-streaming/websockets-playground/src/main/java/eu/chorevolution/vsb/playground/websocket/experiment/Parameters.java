package eu.chorevolution.vsb.playground.websocket.experiment;

public class Parameters {

	public static double msgSendParam = 1;

	// 6 hours
//	public static long experimentDuration = 21600000;
	// 2 hours
//	public static long experimentDuration = 7200000;
	// 1,5 hour
//	public static long experimentDuration = 5400000;
	// 1 hour
//	public static long experimentDuration = 3600000;
	// 30 min
//	public static long experimentDuration = 1800000;
	// 5 min
	public static long experimentDuration = 300000;
	// 1 min
//	public static long experimentDuration = 60000;
	
	 
}
