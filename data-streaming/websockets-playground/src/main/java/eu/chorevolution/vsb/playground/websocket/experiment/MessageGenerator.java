package eu.chorevolution.vsb.playground.websocket.experiment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.instrument.Instrumentation;

import org.apache.commons.text.RandomStringGenerator;
import org.json.simple.JSONObject;

public class MessageGenerator {

	public static long length;
	public static long message_id = 0;
	private static String message;
	private static int payload_size = 195;
	// Message_size Expected of 284 bytes

	public MessageGenerator() {

		message_id = 0;
	}

	public static String getMessage() {

		RandomGeneratedInputStream generator = new RandomGeneratedInputStream(284);
		StringBuilder paylod = new StringBuilder();
		
		paylod.append("afbcdef");
		paylod.append(randomStringMessage(payload_size));
		JSONObject data = new JSONObject();
		message_id++;

		data.put("op_name", "datastream");
		data.put("message_id", String.valueOf((message_id)));
		data.put("payload", paylod.toString());
		return data.toJSONString();
	}
	
	
	public static String getLastMessage() {

		RandomGeneratedInputStream generator = new RandomGeneratedInputStream(284);
		StringBuilder paylod = new StringBuilder();
		
		paylod.append(randomStringMessage(payload_size));
		paylod.append("-finish");
		JSONObject data = new JSONObject();
		message_id++;

		
		data.put("op_name", "datastream");
		data.put("message_id", String.valueOf((message_id)));
		data.put("payload", paylod.toString());
		return data.toJSONString();
	}

	
	public static void main(String arg[]) {
		
		String message = getLastMessage();
		System.out.println(message);
		System.out.println("getMessageLength "+getMessageLength(message));
		
	}
	
	
	public static String randomStringMessage(int messageSize){
		
		
		RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('a', 'z')
				.build();
		
		return generator.generate(messageSize);

	}

	public static int getMessageLength(String message) {
		int length = 0;
		ByteArrayOutputStream ostream = new ByteArrayOutputStream();
		try {
			ObjectOutputStream obStream = new ObjectOutputStream(ostream);
			obStream.writeObject(message);
			byte[] rawObject = ostream.toByteArray();
			ostream.close();
			length = rawObject.length;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return length;
	}
}
