/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.chorevolution.vsb.playgrounds.str.websockets.test.utils;

public class Parameters {
    
    public static double msgPostParam = 01.5;
    public static double onParam = 0.05;
    public static double offParam = 0.05;

    
//    6 hours
//    static long duration = 21600000;
//    2 hours
//    static long duration = 7200000;
    //1,5 hour
//    static long duration = 5400000;
//    1 hour
    public static long experimentDuration = 3600000;
//    30 min
//    public static long experimentDuration = 1800000;
//    5 min
    //static long duration = 300000;
//    3 min
    //static long duration = 60000;

}
