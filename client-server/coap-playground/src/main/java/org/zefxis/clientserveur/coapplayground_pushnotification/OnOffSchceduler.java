package org.zefxis.clientserveur.coapplayground_pushnotification;

import org.eclipse.californium.core.network.config.NetworkConfig;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

public class OnOffSchceduler extends Thread {

	private long ON_TIME = 0l;
	private long OFF_TIME = 0l;
	private int INPUT_MESSAGE_RATE = 300;
	private int NUMBER_OF_UNIQUE_MESSAGE = 65536;
	private boolean timeOn = false;
	private boolean timeOff = false;
	private boolean startScheduler = true;

	public  OnOffSchceduler(NetworkConfig config) {

		ON_TIME = NUMBER_OF_UNIQUE_MESSAGE / INPUT_MESSAGE_RATE;
		OFF_TIME = config.getLong(NetworkConfig.Keys.EXCHANGE_LIFETIME) - ON_TIME;

	}

	public void run() {
		
		setTimeOn(true);
		setTimeOff(false);
		
		while (startScheduler) {

			try {

				if (isTimeOn()) {
					
					System.out.println("ON TIME");
					Thread.sleep(ON_TIME * 1000);
					setTimeOn(false);
					setTimeOff(true);
				}else{
					
					if(OFF_TIME > 0) {
						
						System.out.println("OFF TIME");
						Thread.sleep(OFF_TIME * 1000);
						setTimeOn(true);
						setTimeOff(false);
						synchronized(this){
							
							this.notify();
						}
						
						
					}
					
					
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

	}

	public boolean isTimeOn() {
		return timeOn;
	}

	public void setTimeOn(boolean timeOn) {
		this.timeOn = timeOn;
	}

	public boolean isTimeOff() {
		return timeOff;
	}

	public void setTimeOff(boolean timeOff) {
		this.timeOff = timeOff;
	}

}
