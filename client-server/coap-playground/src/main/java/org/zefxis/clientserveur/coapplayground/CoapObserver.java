package org.zefxis.clientserveur.coapplayground;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



public class CoapObserver {

	public static boolean getResponse = false;
	private static BlockingQueue<String> waitingQueue = null;
	public static double slidingTime = 0l;
	public static long timeWindow = 10; // secondes
	public static int msgCounter = 0;
	public static double throughput = 0.0;
	public static JSONParser parser = new JSONParser();
	public static JSONObject jsonObject = null;
	public static String experimentName = "StartExperimentFRIDAY08032019_WC_85_2_30";
	public static boolean experimentRunning = true;

	public static void main(String args[]) {

		waitingQueue = new LinkedBlockingDeque<String>();

		CoapClient client = new CoapClient("coap://128.93.64.1:8893/datastream").useNONs();

		client = client.useExecutor();

		DynamicThroughput dynamicThroughput = new DynamicThroughput();
		Consummer consummer = new Consummer(waitingQueue, experimentName);
		consummer.start();
		dynamicThroughput.start();
		
		
		CoapObserveRelation relation = client.observe(

				new CoapHandler() {
					@Override
					public void onLoad(CoapResponse response) {

						String content = response.getResponseText();
						if (content.equals("")) {

							content = null;
						}
						
						long arrival_time = System.nanoTime();
						if (content != null) {

							msgCounter++;

							try {

								jsonObject = (JSONObject) parser.parse(content.trim());
								jsonObject.put("arrival_time", ""+arrival_time+"");
								waitingQueue.put(jsonObject.toJSONString());
								String payload = jsonObject.get("payload").toString();
								String[] payloadArray = payload.split("-");
								if (payloadArray.length == 2) {
									
									
									if (payloadArray[1].equals("finish")){

										experimentRunning = false;
									}

								}

							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();

							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}

					@Override
					public void onError() {

						System.err.println("OBSERVING FAILED (press enter to exit)");

					}
				});
		
		
		while(!relation.isCanceled()) {
			
			
			synchronized (relation) {
				
				try {
					
					relation.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		

	}

}
