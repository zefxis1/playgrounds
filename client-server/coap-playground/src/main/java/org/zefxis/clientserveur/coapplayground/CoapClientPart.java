package org.zefxis.clientserveur.coapplayground;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.network.config.NetworkConfig;

/**
 *
 * @author Georgios Bouloukakis (boulouk@gmail.com)
 * Created: Oct 14, 2015
 * Description: 
 */
public class CoapClientPart {
    
    private CoapClient client;
    private Boolean clientOnline = false;
    
    public CoapClientPart() {
        
    	System.out.println(" NetworkConfig.Keys.PROTOCOL_STAGE_THREAD_COUNT "+NetworkConfig.Keys.PROTOCOL_STAGE_THREAD_COUNT);
    }
    
    public void startClient (String destination, String resource) {
//        if(!clientOnline) {
            // Create a client
            client = new CoapClient(destination+resource).useNONs();
//        }
    }
    
    public void post_oneway(String destination, String scope, String dataPost, long lease){
        
    	
    	startClient(destination, scope);
    	client.setURI(destination+"/"+scope);
        System.err.println("Client sent:" + dataPost);
        CoapResponse response = client.post(dataPost, MediaTypeRegistry.APPLICATION_JSON);
        xtget(response);
    }
    
    public void post_twoway(String destination, String scope, String dataPost, long lease){
        
    	startClient(destination, scope);
        client.setURI(destination+"/"+scope);   
        System.err.println("Client requested:" + dataPost);
        CoapResponse response = client.post(dataPost, MediaTypeRegistry.TEXT_PLAIN);
        
        
//        CoapResponse response = client.get(MediaTypeRegistry.TEXT_PLAIN);
        xtget(response);
    }
    
    public void xtget(Object response) {
      if (response instanceof CoapResponse)
        System.err.println("Client responded:" + ((CoapResponse)response).getResponseText());
    }
    
    
    public static void main(String[] args){
    	
    	int count = 0;
    	while(true){
    		
    		CoapClientPart client = new CoapClientPart();
    		
        	//client.startClient("coap://127.0.0.1:9082", "listener");
//            client.post_oneway("coap://127.0.0.1:8891", "listener", "post_oneway hello "+count, 10);
            count++;
            client.post_oneway("coap://127.0.0.1:8891", "listener", "post_oneway hello "+count, 10);
//        	client.post_twoway("coap://127.0.0.1:8891", "listener", "post_twoway hello"+count, 100);
    		try {
				
    			Thread.sleep(1);
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
	}
		   
}
