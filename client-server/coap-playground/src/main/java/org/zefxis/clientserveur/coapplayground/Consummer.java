package org.zefxis.clientserveur.coapplayground;

import java.util.concurrent.BlockingQueue;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

import fr.inria.mimove.monitor.persistence.MongoDB;

public class Consummer extends Thread{
	
	private BlockingQueue<String> waitingQueue = null;
	private JSONParser parser = new JSONParser();
	private JSONObject jsonObject = null;
	private String experimentName = null;
	public Consummer(BlockingQueue<String> waitingQueue, String experimentName){
		
		
		this.waitingQueue = waitingQueue;
		this.experimentName = experimentName;
		
	}
	
	public void run() {
		
		
		double local_value = CoapObserver.throughput;
		while(CoapObserver.experimentRunning) {
			
			try {
				
				String data = waitingQueue.take();
				
				if(data != null) {
					
					jsonObject = (JSONObject) parser.parse(data.trim());
					BasicDBObject newDocument = (BasicDBObject) JSON.parse(jsonObject.toString().trim());
					MongoDB.getCollection(experimentName+"_data").insert(newDocument).getN();
					
					
				}
			} catch (InterruptedException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
	}
	
	
}
