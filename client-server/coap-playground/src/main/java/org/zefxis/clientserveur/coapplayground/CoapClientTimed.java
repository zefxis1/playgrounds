package org.zefxis.clientserveur.coapplayground;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;

public class CoapClientTimed {

	public static boolean getResponse = false;
	public static String resource = "helloWorldResource";
	public static String host_address = "127.0.0.1";
	public static String port_number = "8891";

	public static CoapObserveRelation relation = null;

	public static void main(String args[]) {

		CoapClient client = new CoapClient("coap://" + host_address + ":" + port_number + "/"+resource).useNONs();

		client = client.useExecutor();

		// Initialize Time interval
		long timeInterval = 5 * 1000; // 5 seconds

		relation = client.observe(

				new CoapHandler() {
					@Override
					public void onLoad(CoapResponse response) {

						String content = response.getResponseText();
						if (content.equals(null)) {

							content = "";
						}
						System.out.println(" DataStream " + content);

					}

					@Override
					public void onError() {

						System.err.println("OBSERVING FAILED (press enter to exit)");

					}
				});

		// set timer thread
		setTimer(timeInterval);
		System.out.println("set timer thread");

		// Start monitoring the relation with the server
		System.out.println("Start monitoring the relation with the server");
		while (!relation.isCanceled()) {

			synchronized (relation) {

				try {

					relation.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.err.println("Relation is cancelled");
		System.exit(0);

	}

	public static void setTimer(long timeInterval) {

		new java.util.Timer().schedule(new java.util.TimerTask() {

			@Override
			public void run() {

				synchronized (relation) {

					// If the time interval elapsed, cancel the relation with server
					System.out.println("The time interval elapsed, cancel the relation with server");
					relation.reactiveCancel();
					relation.notify();
				}

			}
		}, timeInterval);

	}

}
