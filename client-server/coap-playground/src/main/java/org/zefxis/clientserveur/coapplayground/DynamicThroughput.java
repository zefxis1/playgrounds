package org.zefxis.clientserveur.coapplayground;

import com.mongodb.BasicDBObject;

import fr.inria.mimove.monitor.persistence.MongoDB;

public class DynamicThroughput extends Thread {

	public void DynamicThroughput() {
	}

	public void run() {

		while (CoapObserver.experimentRunning) {

			try {

				Thread.sleep(CoapObserver.timeWindow * 1000);

				CoapObserver.throughput = CoapObserver.msgCounter / CoapObserver.timeWindow;
				CoapObserver.msgCounter = 0;
				CoapObserver.slidingTime += CoapObserver.timeWindow;
				BasicDBObject throughputDoc = new BasicDBObject();
				throughputDoc.put("throughput", CoapObserver.throughput);
				throughputDoc.put("slidingTime", CoapObserver.slidingTime);
				MongoDB.getCollection(CoapObserver.experimentName + "_throughput").insert(throughputDoc).getN();
				System.out.println(
						"throughput :" + CoapObserver.throughput + " slidingTime : " + CoapObserver.slidingTime);

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
