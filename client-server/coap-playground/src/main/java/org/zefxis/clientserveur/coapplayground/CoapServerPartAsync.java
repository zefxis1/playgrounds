package org.zefxis.clientserveur.coapplayground; 
 
import java.net.InetSocketAddress; 
import java.net.SocketException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResource; 
import org.eclipse.californium.core.CoapServer; 
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.server.resources.CoapExchange; 
 
 
public class CoapServerPartAsync { 
 
 private static final int COAP_PORT = 8891;// NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_PORT); 
 private static int incrementor = 1; 
 private static HelloWorldResource helloWorldResource = null;
 private ScheduledExecutorService executor = null;
 /*
     * Application entry point. 
     */ 
    public static void main(String[] args) { 
         
        try { 
 
            // create server 
        	
        	NetworkConfig config = NetworkConfig.getStandard().setInt(NetworkConfig.Keys.PROTOCOL_STAGE_THREAD_COUNT, 10)
                    .setString(NetworkConfig.Keys.DEDUPLICATOR, "NO_DEDUPLICATOR");
        	
        	CoapServer coapServer = new CoapServer(config, 8891);
        	
        	coapServer.addEndpoint(addEndpoints());
            
        	helloWorldResource = new HelloWorldResource("helloWorldResource",true);
        	coapServer.add(helloWorldResource); 
            
            coapServer.start(); 
            int count = 1;
            while(true){
            	
            	
            	sendNotification("helloWorldResource-"+count++);
            	try {
					
            		Thread.sleep(1);
					
				} catch (InterruptedException e) {e.printStackTrace();}
            	
            }
 
        } catch (SocketException e) { 
            System.err.println("Failed to initialize server: " + e.getMessage()); 
        } 
    } 
 
    /**
     * Add endpoints listening on default CoAP port on all IP addresses of all network interfaces. 
     *  
     * @throws SocketException if network interfaces cannot be determined 
     */ 
    private static CoapEndpoint addEndpoints() throws SocketException { 
        InetSocketAddress bindToAddress = new InetSocketAddress("127.0.0.1", COAP_PORT); 
     return new CoapEndpoint(bindToAddress); 
      
    } 
   
    
    public static void sendNotification(String message){
    	
    	
    	helloWorldResource.changed(message);
    }
    
    
    
 
 
}
