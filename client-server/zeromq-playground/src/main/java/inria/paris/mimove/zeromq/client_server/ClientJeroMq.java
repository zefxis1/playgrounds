package inria.paris.mimove.zeromq.client_server;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

public class ClientJeroMq {
	


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ZMQ.Context context = ZMQ.context(1);

        //  Socket to talk to server
        System.out.println("Connecting to hello world server…");

        ZMQ.Socket client = context.socket(ZMQ.REQ);
        int requestNbr = 0;
        client.connect("tcp://localhost:5555");
        while(!Thread.currentThread().isInterrupted()){
        
            String request = "Hello " + requestNbr;
            System.out.println("Sending Hello " + requestNbr);
            client.send(request.getBytes(), 0);

            byte[] reply = client.recv(0);
            System.out.println("Received " + new String(reply) + " " + requestNbr);
            requestNbr++;
            
        }
        client.close();
        context.term();
	}
	
	
}
