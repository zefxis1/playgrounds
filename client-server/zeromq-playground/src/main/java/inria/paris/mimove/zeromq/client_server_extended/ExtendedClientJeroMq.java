package inria.paris.mimove.zeromq.client_server_extended;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

public class ExtendedClientJeroMq {
	


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Client client1 = new Client("client1");
		Client client2 = new Client("client2");
		Client client3 = new Client("client3");
		Client client4 = new Client("client4");
		Client client5 = new Client("client5");
		Client client6 = new Client("client6");
		Client client7 = new Client("client7");
		client1.start();
		client2.start();
		client3.start();
		client4.start();
		client5.start();
		client6.start();
		client7.start();
		
	}
	
	private static class Client extends Thread
    {
        private Context context;
        private String name = null;

        private Client (String name)
        {
            this.context = ZMQ.context(1);
            this.name = name;
        }

        @Override
        public void run(){
           

            //  Socket to talk to server
            System.out.println("Connecting to ROUTER…");

            ZMQ.Socket requester = context.socket(ZMQ.REQ);
            int requestNbr = 0;
            requester.connect("tcp://localhost:5559");
            while(!Thread.currentThread().isInterrupted()){
            
                String request = name+" says Hello " + requestNbr;
                System.out.println(name+" sending Hello " + requestNbr);
                requester.send(request.getBytes(), 0);

                byte[] reply = requester.recv(0);
                System.out.println(name+" received " + new String(reply) + " " + requestNbr);
                requestNbr++;
                
            }
            requester.close();
            context.term();
        }

    }
	
	
	
}
