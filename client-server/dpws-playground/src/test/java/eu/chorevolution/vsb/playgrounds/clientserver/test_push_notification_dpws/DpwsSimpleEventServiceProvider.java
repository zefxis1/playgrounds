package eu.chorevolution.vsb.playgrounds.clientserver.test_push_notification_dpws;

import java.io.IOException;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.eventing.EventSource;
import org.ws4d.java.types.QName;

import eu.chorevolution.vsb.gm.protocols.dpws.DPWSDevice;

public class DpwsSimpleEventServiceProvider {

	public static void main(String[] args){
		// TODO Auto-generated method stub
		
		JMEDSFramework.start(args);
		DPWSDevice device = new DPWSDevice();
		DpwsSimpleEventService eventService = new DpwsSimpleEventService();
		EventSource eventSource = eventService.getEventSource(new QName("BasicServices", DpwsSimpleEvent.DOCU_NAMESPACE), "DpwsSimpleEvent", null, "DpwsSimpleEvent");
		DpwsSimpleEventProvider eventProvider = new  DpwsSimpleEventProvider((DpwsSimpleEvent)eventSource);
		eventProvider.start();
		device.addService(eventService);
		try{
			device.start();
		}catch (IOException e) {e.printStackTrace();}
		
		
	}

}
