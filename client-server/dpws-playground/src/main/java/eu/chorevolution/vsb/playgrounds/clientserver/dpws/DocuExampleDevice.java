package eu.chorevolution.vsb.playgrounds.clientserver.dpws;

import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;

public class DocuExampleDevice extends DefaultDevice{
	
	public final static String	DOCU_NAMESPACE	= "http://ws4d.org/jmeds";
	
	public DocuExampleDevice(){
		
		super();
		
		this.setPortTypes(new QNameSet(new QName("DocuExampleDevice", DOCU_NAMESPACE)));
		this.addFriendlyName("en-US", "DocuDevice");
		this.addFriendlyName(LocalizedString.LANGUAGE_DE, "DokuGeraet");
		
		this.addManufacturer(LocalizedString.LANGUAGE_EN, "Test Inc.");
		this.addManufacturer("de-DE", "Test GmbH");
		
		this.addModelName(LocalizedString.LANGUAGE_EN, "DocuModel");
//		
//		NetworkInterface iface = IPNetworkDetection.getInstance().getNetworkInterface("eno1");
//		IPDiscoveryDomain domain = IPNetworkDetection.getInstance().getIPDiscoveryDomainForInterface(iface, false);
//		this.addBinding(new IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, domain));
	}
}
