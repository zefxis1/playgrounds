/*package eu.chorevolution.vsb.playgrounds.clientserver.rest.experiment;

import eu.chorevolution.vsb.experiment.utils.Exp;
import eu.chorevolution.vsb.experiment.utils.MessageGenerator;

public class StartSourceApplication extends Thread {

	private StartRestClient sender = null;
	private String msg = "";
	private Exp waitDuration;
	private long averageMsgSize = 0l;
	private MessageGenerator msgGen = null;

	public StartSourceApplication(StartRestClient sender, Exp waitDuration, long averageMsgSize,  MessageGenerator msgGen) {

		this.sender = sender;
		this.waitDuration = waitDuration;
		this.averageMsgSize = averageMsgSize;
		this.msgGen = msgGen;

	}

	public void run() {
		
		System.out.println("Thread sender started");
		int count = 0;
		while (StartExperiment.experimentRunning) {

			synchronized (sender){

				sender.send(msgGen.getMessage());
				StartExperiment.msgCounter++;
			}
			
				
				try {
//					this.sleep(1000);
					double delay = waitDuration.next() * 1000; 
					this.sleep((long)delay);

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
		this.interrupt();
		System.out.println("StartSourceApplication-" + this.getId() + " finish the job and dies ");
	}


}
*/