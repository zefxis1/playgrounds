/*package eu.chorevolution.vsb.playgrounds.clientserver.rest.experiment;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.restlet.Component;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Protocol;
import org.restlet.data.Status;

import eu.chorevolution.vsb.agent.MeasureAgent;
import eu.chorevolution.vsb.monitor.util.MonitorConstant;

public class RestletRestService extends Restlet {

	private ExecutorService executor = null;
	private static MeasureAgent agent = null;
	
	public static void main(String[] args) throws Exception {
		// Create the HTTP server and listen on port 8182
		// Create a new Restlet component and add a HTTP server connector to it
		Component component = new Component();

		component.getServers().add(Protocol.HTTP, 8080);
		component.getDefaultHost().attach("/", new RestletRestService());

		component.getServers().get(0).getContext().getParameters().add("maxTotalConnections", "-1");
		component.getServers().get(0).getContext().getParameters().add("maxThreads", "100");
		component.start();
		agent = new MeasureAgent("timestamp_2",System.currentTimeMillis(),MonitorConstant.MDefault,MonitorConstant.timestamp_2_port_listener);
	}

	public RestletRestService(){

		executor = Executors.newFixedThreadPool(10);

	}

	@Override
	public void handle(Request request, Response response) {

		if (request.getMethod().equals(Method.POST)) {

			try {
				String str = request.getEntity().getText().toString();
				String message_id = str.split("-")[1];
            	agent.fire(""+System.currentTimeMillis()+"-"+message_id);
				System.out.println(" reciveive " + str);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.setAutoCommitting(false);
			final Response resp = response;
			response.setAutoCommitting(false);
			resp.setStatus(Status.SUCCESS_OK);
			resp.setEntity("OK Receive:)",
			MediaType.TEXT_PLAIN);
			resp.commit();
			return;
					}

	}

}
*/