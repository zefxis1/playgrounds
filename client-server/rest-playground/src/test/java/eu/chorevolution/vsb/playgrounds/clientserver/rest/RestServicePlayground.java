package eu.chorevolution.vsb.playgrounds.clientserver.rest;


import java.io.IOException;
import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;



public class RestServicePlayground {

  private Component component;
  private Server server;  

  public RestServicePlayground(final int port) {
    this.server = new Server(Protocol.HTTP, port);
    this.component = new Component();
    this.component.getServers().add(server);
    this.component.getDefaultHost().attach("/bridgeNextClosure", InterfaceDescPost.class);
    this.component.getDefaultHost().attach("/bridgeNextClosure/lat={lat}&lon={lon}", InterfaceDescGet.class);
    try {
      this.component.start();
    } catch (Exception e){
    	
      e.printStackTrace();
    }
  }

  public static class InterfaceDescGet extends ServerResource {
    @Override
    protected Representation get() throws ResourceException {
      String lat = (String) this.getRequestAttributes().get("lat");
      String lon = (String) this.getRequestAttributes().get("lon");
      System.out.println("lat=" + lat + " lon=" + lon);

           String returnMessage = "Request forwarded!";
 
      return new StringRepresentation(returnMessage);
    }
  }

  public static class InterfaceDescPost extends ServerResource {
    @Override
    protected Representation post(Representation entity) throws ResourceException {
      String receivedText = null;
      try {
        receivedText = entity.getText();
      } catch (IOException e1) {
        e1.printStackTrace();
      }

      System.out.println("rec: " + receivedText);

      String returnMessage = "";
      returnMessage = "Received!";
      return new StringRepresentation(returnMessage);
    }
  }

  public static void main(String[] args) {
	  
	  RestServicePlayground restServicePlayground = new RestServicePlayground(1111);
  }

}